﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481HW4
{
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<ImageCellItem> imagesForListView { get; set; }

        public MainPage()
        {
            InitializeComponent();

            WarcraftList.ItemsSource = imagesForListView;

            PopulateListView();
        }

        public void PopulateListView()
        {
            var imagesForListView = new ObservableCollection<ImageCellItem>()
            {
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("warlock.png"),
                    ImageText = "Warlock",
                    Description = "Destructive spellcaster",
                    url = "https://worldofwarcraft.com/en-us/game/classes/warlock",
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("druid.png"),
                    ImageText = "Druid",
                    Description = "Versitile shapeshifter",
                    url = "https://worldofwarcraft.com/en-us/game/classes/druid",
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("monk.png"),
                    ImageText = "Monk",
                    Description = "Drunken martial artist",
                    url = "https://worldofwarcraft.com/en-us/game/classes/monk",
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("mage.png"),
                    ImageText = "Mage",
                    Description = "Master of frost, fire and arcane",
                    url = "https://worldofwarcraft.com/en-us/game/classes/mage",
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("priest.png"),
                    ImageText = "Priest",
                    Description = "Heals allies with holy magic",
                    url = "https://worldofwarcraft.com/en-us/game/classes/priest",
                },
            };
            WarcraftList.ItemsSource = imagesForListView;                   
        }

        private void Handle_MoreButton(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var war = (ImageCellItem)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreInfoPage(war));
        }
        private void Handle_Delete(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            var wowClass = (ImageCellItem)menuItem.CommandParameter;
            imagesForListView.Remove(wowClass);
        }
    }
}
