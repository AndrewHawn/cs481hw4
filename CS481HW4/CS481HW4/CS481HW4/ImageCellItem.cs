﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CS481HW4
{
    public class ImageCellItem
    {
        public string ImageText
        {
            get;
            set;
        }
        public ImageSource IconSource
        {
            get;
            set;
        }
        public string url
        {
            get;
            set;
        }
        public string Description
        {
            get;
            set;
        }
    }
}
