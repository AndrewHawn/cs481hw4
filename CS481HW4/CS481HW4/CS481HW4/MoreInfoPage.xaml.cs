﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481HW4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreInfoPage : ContentPage
	{
		public MoreInfoPage ()
		{
			InitializeComponent ();
		}
        public MoreInfoPage(ImageCellItem war)
        {
            InitializeComponent();
            BindingContext = war;
        }
	}
}